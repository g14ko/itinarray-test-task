/**
 * INSPINIA - Responsive Admin Theme
 *
 * Main directives.js file
 * Define directives for used plugin
 *
 *
 * Functions (directives)
 *  - sideNavigation
 *  - iboxTools
 *  - minimalizaSidebar
 *  - vectorMap
 *  - sparkline
 *  - icheck
 *  - ionRangeSlider
 *  - dropZone
 *  - responsiveVideo
 *  - chatSlimScroll
 *  - customValid
 *  - fullScroll
 *  - closeOffCanvas
 *  - clockPicker
 *  - landingScrollspy
 *  - fitHeight
 *  - iboxToolsFullScreen
 *  - slimScroll
 *  - truncate
 *  - touchSpin
 *  - markdownEditor
 *  - resizeable
 *
 */

/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
function sideNavigation($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element) {
            // Call the metsiMenu plugin and plug it to sidebar navigation
            $timeout(function () {
                element.metisMenu();

            });
        }
    };
};

/**
 * responsibleVideo - Directive for responsive video
 */
function responsiveVideo() {
    return {
        restrict: 'A',
        link: function (scope, element) {
            var figure = element;
            var video = element.children();
            video
                .attr('data-aspectRatio', video.height() / video.width())
                .removeAttr('height')
                .removeAttr('width')

            //We can use $watch on $window.innerWidth also.
            $(window).resize(function () {
                var newWidth = figure.width();
                video
                    .width(newWidth)
                    .height(newWidth * video.attr('data-aspectRatio'));
            }).resize();
        }
    }
}

/**
 * iboxTools - Directive for iBox tools elements in right corner of ibox
 */
function iboxTools($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/ibox_tools.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            };
            // Function for close ibox
            $scope.closebox = function () {
                var ibox = $element.closest('div.ibox');
                ibox.remove();
            }
        }
    };
}

/**
 * iboxTools with full screen - Directive for iBox tools elements in right corner of ibox with full screen option
 */
function iboxToolsFullScreen($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/ibox_tools_full_screen.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            };
            // Function for close ibox
            $scope.closebox = function () {
                var ibox = $element.closest('div.ibox');
                ibox.remove();
            };
            // Function for full screen
            $scope.fullscreen = function () {
                var ibox = $element.closest('div.ibox');
                var button = $element.find('i.fa-expand');
                $('body').toggleClass('fullscreen-ibox-mode');
                button.toggleClass('fa-expand').toggleClass('fa-compress');
                ibox.toggleClass('fullscreen');
                setTimeout(function () {
                    $(window).trigger('resize');
                }, 100);
            }
        }
    };
}

/**
 * minimalizaSidebar - Directive for minimalize sidebar
 */
function minimalizaSidebar($timeout) {
    return {
        restrict: 'A',
        template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-default" href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
        controller: function ($scope, $element) {
            $scope.minimalize = function () {
                var
                    body = $("body"),
                    menu = $("#side-menu");
                body.toggleClass("mini-navbar");
                switch (true) {
                    case !body.hasClass('mini-navbar') || body.hasClass('body-small'):
                        // Hide menu in order to smoothly turn on when maximize menu
                        menu.hide();
                        // For smoothly turn on menu
                        setTimeout(function () {
                            menu.fadeIn(400);
                        }, 200);
                        break;
                    case body.hasClass('fixed-sidebar'):
                        menu.hide();
                        setTimeout(function () {
                            menu.fadeIn(400);
                        }, 100);
                        break;
                    default:
                        // Remove all inline style from jquery fadeIn function to reset menu state
                        menu.removeAttr('style');
                }
            }
        }
    };
};


function closeOffCanvas() {
    return {
        restrict: 'A',
        template: '<a class="close-canvas-menu" ng-click="closeOffCanvas()"><i class="fa fa-times"></i></a>',
        controller: function ($scope, $element) {
            $scope.closeOffCanvas = function () {
                $("body").toggleClass("mini-navbar");
            }
        }
    };
}

/**
 * vectorMap - Directive for Vector map plugin
 */
function vectorMap() {
    return {
        restrict: 'A',
        scope: {
            myMapData: '=',
        },
        link: function (scope, element, attrs) {
            element.vectorMap({
                map: 'world_mill_en',
                backgroundColor: "transparent",
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 0.9,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 0
                    }
                },
                series: {
                    regions: [
                        {
                            values: scope.myMapData,
                            scale: ["#1ab394", "#22d6b1"],
                            normalizeFunction: 'polynomial'
                        }
                    ]
                },
            });
        }
    }
}


/**
 * sparkline - Directive for Sparkline chart
 */
function sparkline() {
    return {
        restrict: 'A',
        scope: {
            sparkData: '=',
            sparkOptions: '=',
        },
        link: function (scope, element, attrs) {
            scope.$watch(scope.sparkData, function () {
                render();
            });
            scope.$watch(scope.sparkOptions, function () {
                render();
            });
            var render = function () {
                $(element).sparkline(scope.sparkData, scope.sparkOptions);
            };
        }
    }
};

/**
 * icheck - Directive for custom checkbox icheck
 */
function icheck($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function ($scope, element, $attrs, ngModel) {
            return $timeout(function () {
                var value;
                value = $attrs['value'];

                $scope.$watch($attrs['ngModel'], function (newValue) {
                    $(element).iCheck('update');
                })

                return $(element).iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green'

                }).on('ifChanged', function (event) {
                    if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                        $scope.$apply(function () {
                            return ngModel.$setViewValue(event.target.checked);
                        });
                    }
                    if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                        return $scope.$apply(function () {
                            return ngModel.$setViewValue(value);
                        });
                    }
                });
            });
        }
    };
}

/**
 * ionRangeSlider - Directive for Ion Range Slider
 */
function ionRangeSlider() {
    return {
        restrict: 'A',
        scope: {
            rangeOptions: '='
        },
        link: function (scope, elem, attrs) {
            elem.ionRangeSlider(scope.rangeOptions);
        }
    }
}

/**
 * dropZone - Directive for Drag and drop zone file upload plugin
 */


/**
 * chatSlimScroll - Directive for slim scroll for small chat
 */
function chatSlimScroll($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element) {
            $timeout(function () {
                element.slimscroll({
                    height: '234px',
                    railOpacity: 0.4
                });

            });
        }
    };
}

/**
 * customValid - Directive for custom validation example
 */
function customValid() {
    return {
        require: 'ngModel',
        link: function (scope, ele, attrs, c) {
            scope.$watch(attrs.ngModel, function () {

                // You can call a $http method here
                // Or create custom validation

                var validText = "Inspinia";

                if (scope.extras == validText) {
                    c.$setValidity('cvalid', true);
                } else {
                    c.$setValidity('cvalid', false);
                }

            });
        }
    }
}


/**
 * fullScroll - Directive for slimScroll with 100%
 */
function fullScroll($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element) {
            $timeout(function () {
                element.slimscroll({
                    height: '100%',
                    railOpacity: 0.9
                });

            });
        }
    };
}

/**
 * slimScroll - Directive for slimScroll with custom height
 */
function slimScroll($timeout) {
    return {
        restrict: 'A',
        scope: {
            boxHeight: '@'
        },
        link: function (scope, element) {
            $timeout(function () {
                element.slimscroll({
                    height: scope.boxHeight,
                    railOpacity: 0.9
                });

            });
        }
    };
}

/**
 * clockPicker - Directive for clock picker plugin
 */
function clockPicker() {
    return {
        restrict: 'A',
        link: function (scope, element) {
            element.clockpicker();
        }
    };
};


/**
 * landingScrollspy - Directive for scrollspy in landing page
 */
function landingScrollspy() {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.scrollspy({
                target: '.navbar-fixed-top',
                offset: 80
            });
        }
    }
}

/**
 * fitHeight - Directive for set height fit to window height
 */
function fitHeight() {
    return {
        restrict: 'A',
        link: function (scope, element) {
            element.css("height", $(window).height() + "px");
            element.css("min-height", $(window).height() + "px");
        }
    };
}

/**
 * truncate - Directive for truncate string
 */
function truncate($timeout) {
    return {
        restrict: 'A',
        scope: {
            truncateOptions: '='
        },
        link: function (scope, element) {
            $timeout(function () {
                element.dotdotdot(scope.truncateOptions);

            });
        }
    };
}


/**
 * touchSpin - Directive for Bootstrap TouchSpin
 */
function touchSpin() {
    return {
        restrict: 'A',
        scope: {
            spinOptions: '='
        },
        link: function (scope, element, attrs) {
            scope.$watch(scope.spinOptions, function () {
                render();
            });
            var render = function () {
                $(element).TouchSpin(scope.spinOptions);
            };
        }
    }
};

/**
 * markdownEditor - Directive for Bootstrap Markdown
 */
function markdownEditor() {
    return {
        restrict: "A",
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            $(element).markdown({
                savable: false,
                onChange: function (e) {
                    ngModel.$setViewValue(e.getContent());
                }
            });
        }
    }
};

/**
 *
 * Pass all functions into module
 */
angular.module('technical-task')
    .directive('pageTitle', function ($rootScope, $timeout) {
        return {
            link: function (scope, element, attributes) {
                $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                    var title = [attributes.pageTitle];
                    if (toState.data && toState.data.pageTitle){
                        title.push(toState.data.pageTitle);
                    }

                    console.log(title);

                    $timeout(function () {
                        element.text(title.join(' | '));
                    });
                });
            }
        }
    })
    .directive('sideNavigation', sideNavigation)
    .directive('iboxTools', iboxTools)
    .directive('minimalizaSidebar', minimalizaSidebar)
    .directive('vectorMap', vectorMap)
    .directive('sparkline', sparkline)
    .directive('icheck', icheck)
    .directive('ionRangeSlider', ionRangeSlider)
    .directive('responsiveVideo', responsiveVideo)
    .directive('chatSlimScroll', chatSlimScroll)
    .directive('customValid', customValid)
    .directive('fullScroll', fullScroll)
    .directive('closeOffCanvas', closeOffCanvas)
    .directive('clockPicker', clockPicker)
    .directive('landingScrollspy', landingScrollspy)
    .directive('fitHeight', fitHeight)
    .directive('iboxToolsFullScreen', iboxToolsFullScreen)
    .directive('slimScroll', slimScroll)
    .directive('truncate', truncate)
    .directive('touchSpin', touchSpin)
    .directive('markdownEditor', markdownEditor)
    .directive('dropZone', function () {
        return {
            restrict: 'E',
            scope: {
                config: '=',
                success: '&',
                error: '&'
            },
            template: '<div class="btn-group pull-right"><div class="btn btn-primary" ng-click="upload()">{{"Upload Files" | translate}}</div>' +
            '<div class="btn btn-danger" ng-click="reset()">{{"Clear Files" | translate}}</div></div>',
            link: function (scope, element, attrs) {
                var config = {
                        maxFilesize: 20,
                        paramName: "files",
                        maxThumbnailFilesize: 2,
                        thumbnailWidth: 100,
                        thumbnailHeight: 100,
                        uploadMultiple: true,
                        parallelUploads: 5,
                        autoProcessQueue: false
                    },
                    eventHandlers = {
                        'successmultiple': scope.success(),
                        'error': scope.error()
                    },
                    dropZone = new Dropzone(element[0], angular.extend(config, scope.config));
                dropZone.autoDiscover = false;

                angular.forEach(eventHandlers, function (handler, event) {
                    dropZone.on(event, handler);
                });

                scope.upload = function () {
                    dropZone.processQueue();
                };

                scope.reset = function () {
                    dropZone.removeAllFiles();
                }
            }
        }
    })
    .directive('dtEdit', function ($timeout) {
    return {
        restrict: 'AE',
        scope: {
            dtEdit: '=',
            onSave: '&'
        },
        template: '<span ng-show="!editing" ng-click="doEdit()">{{dtEdit.product_ean || "Not Set" | translate }}</span>' +
        '<p ng-show="dtEdit.eanUpdateError" class="text-danger">{{dtEdit.eanUpdateMessage}}</p>' +
        '<div ng-show="editing" class="input-group"> ' +
        '<input type="text" class="form-control" ng-model="dtEdit.product_ean" ng-focus="dtEdit.eanUpdateError = undefined">' +
        '<div class="input-group-btn"> ' +
        '<button type="button" class="btn btn-default" ng-click="onSave()">{{"Save" | translate}}</button>' +
        '<button type="button" class="btn btn-default" ng-click="cancel()">{{"Cancel" | translate}}</button></div>' +
        '</div>',
        link: function ($scope, element) {
            $scope.editing = false;
            $scope.doEdit = function () {
                $scope.oldValue = angular.copy($scope.dtEdit);
                $scope.editing = true;
                $timeout(function () {
                    angular.element(element[0].lastChild.children[0]).focus();
                }, 10);
            };
            $scope.cancel = function () {
                $scope.dtEdit = angular.copy($scope.oldValue);
                $scope.oldValue = undefined;
                $scope.editing = false;
            };
            $scope.$watch('dtEdit.eanUpdateError', function (res) {
                if (res === false) {
                    $scope.editing = res;
                }
            });
        }
    }
})
    .directive('statsItem', function () {
    return {
        restrict: 'AE',
        scope: {
            config: '=',
            statsItem: '='
        },
        template: '<label>{{config.label | translate}}</label>' +
        '<div ng-if="config.type == \'female\' && config.label !=\'used\'" class="label label-primary max-percentage">{{statsItem[config.type][config.label].percent}} % <i class="fa fa-arrow-right"></i></div>' +
        '<div ng-if="config.type == \'male\' && config.label !=\'used\'" class="label label-success max-percentage">{{statsItem[config.type][config.label].percent}} % <i class="fa fa-arrow-right"></i></div>' +
        '<div ng-if="config.type == \'male\' && config.label !=\'used\'" class="label label-warning median-percentage">{{statsItem[config.type][config.label].percent}} % <i class="fa fa-arrow-right"></i></div>' +
        '<div class="stats-peak">' +
        '<div ng-if="config.type == \'female\'" class="stats-peak-text">{{statsValue | number}}</div>' +
        '<div class="stats-peak-background" style="height:{{peakHeight}}"></div>' +
        '<div ng-if="config.type == \'male\'" class="stats-peak-text">{{statsValue | number}}</div>' +
        '</div>',
        link: function ($scope, element) {
            $scope.$watchCollection('statsItem', function (stats) {
                $scope.statsValue = stats[$scope.config.type][$scope.config.label].count;
                function convert(n) {
                    var order = Math.log(n),
                        base = 1.8;
                    if ((order > 10) && (order < 12 )) {
                        base = 1.6;
                    } else if ((order > 12) && (order < 15 )) {
                        base = 1.45;
                    } else if ((order > 15) && (order < 20 )) {
                        base = 1.35;
                    }
                    return Math.pow(base, order);
                }

                $scope.peakHeight = convert($scope.statsValue) + 'px;';
            })

        }
    }
});