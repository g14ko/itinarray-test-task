angular.module('technical-task')
    .service('AuthService', [
        '$q',
        '$http',
        '$rootScope',
        '$state',
        '$location',
        '$window',
        'localStorageService',
        function ($q, $http, $rootScope, $state, $location, $window, localStorageService) {
            var service = {};

            service.defaultLanguage = 'en-US';

            service.setUser = function (user) {
                return localStorageService.set('user', user);
            };

            service.getUser = function () {
                return localStorageService.get('user');
            };

            service.setLanguages = function (languages) {
                localStorageService.set('languages', languages);
            };

            service.getLanguages = function () {
                return localStorageService.get('languages');
            };

            service.setLanguage = function (language) {
                localStorageService.set('language', language || service.defaultLanguage);
            };

            service.getLanguage = function () {
                return localStorageService.get('language') || service.defaultLanguage;
            };

            service.setProfile = function (profile) {
                localStorageService.set('user-profile', profile);
            };

            service.getProfile = function () {
                return localStorageService.get('user-profile');
            };

            service.setMenu = function (menu) {
                localStorageService.set('user-menu', menu);
            };

            service.getMenu = function () {
                return localStorageService.get('user-menu');
            };

            service.logout = function () {
                localStorageService.remove('user');
                localStorageService.remove('user-profile');
                localStorageService.remove('user-menu');
                localStorageService.remove('languages');
                localStorageService.remove('language');
            };

            return service;
        }
    ]);
