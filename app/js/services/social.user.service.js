angular.module('technical-task')
    .factory('SocialUserService', [
        '$q',
        '$http',
        'envService',
        function ($q, $http, envService) {
            var apiUrl = envService.read('apiUrl');
            var service = {};

            // users

            service.getUsers = function (search) {
                var deferred = $q.defer();

                $http.get(apiUrl + 'social/users', {params: {search: search}})
                    .then(function (response) {
                        deferred.resolve(response.data);
                    }, function (response) {
                        deferred.reject(response.data);
                    });

                return deferred.promise;
            };

            service.create = function (user) {
                var deferred = $q.defer();

                $http.post(apiUrl + 'social/user', user)
                    .then(function (response) {
                        deferred.resolve(response.data);
                    }, function (response) {
                        deferred.reject(response.data);
                    });

                return deferred.promise;
            };

            service.update = function (_id, user) {
                var deferred = $q.defer();

                $http.put(apiUrl + 'social/user/' + _id, user)
                    .then(function (response) {
                        deferred.resolve(response.data);
                    }, function (response) {
                        deferred.reject(response.data);
                    });

                return deferred.promise;
            };

            service.find = function (uid) {
                var deferred = $q.defer();

                $http.get(apiUrl + 'social/user/find/' + uid)
                    .then(function (response) {
                        deferred.resolve(response.data);
                    }, function (response) {
                        deferred.reject(response.data);
                    });

                return deferred.promise;
            };

            return service;
        }
    ]);