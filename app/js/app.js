angular.module('technical-task', [
    'environment',
    'LocalStorageModule',
    'oc.lazyLoad',
    'ui.router',
    'ui.router.modal',
    'ui.bootstrap',
    'pascalprecht.translate',
    'socialLogin'
]);