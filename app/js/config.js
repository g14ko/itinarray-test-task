angular.module('technical-task')
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        '$ocLazyLoadProvider',
        'localStorageServiceProvider',
        '$translateProvider',
        'socialProvider',
        'envServiceProvider',
        function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, localStorageServiceProvider, $translateProvider, socialProvider, envServiceProvider) {

            // set the domains and variables for each environment
            envServiceProvider.config({
                domains: {
                    development: ['localhost', 'acme.dev.local'],
                    production: ['acme.com', '*.acme.com', 'acme.dev.prod'],
                    test: ['test.acme.com', 'acme.dev.test', 'acme.*.com'],
                    // anotherStage: ['domain1', 'domain2']
                },
                vars: {
                    development: {
                        apiUrl: '//itinarray-technical-task.api/',
                        staticUrl: '//static.acme.dev.local',
                        // antoherCustomVar: 'lorem',
                        // antoherCustomVar: 'ipsum'
                    },
                    test: {
                        apiUrl: '//api.acme.dev.test/v1',
                        staticUrl: '//static.acme.dev.test',
                        // antoherCustomVar: 'lorem',
                        // antoherCustomVar: 'ipsum'
                    },
                    production: {
                        apiUrl: '//api.acme.com/v1',
                        staticUrl: '//static.acme.com',
                        // antoherCustomVar: 'lorem',
                        // antoherCustomVar: 'ipsum'
                    },
                    // anotherStage: {
                    // 	customVar: 'lorem',
                    // 	customVar: 'ipsum'
                    // },
                    defaults: {
                        apiUrl: '//api.default.com/v1',
                        staticUrl: '//static.default.com'
                    }
                }
            });

            // socialProvider.setGoogleKey("YOUR GOOGLE CLIENT ID");
            // socialProvider.setLinkedInKey("YOUR LINKEDIN CLIENT ID");
            socialProvider.setFbKey({appId: "761085994064890", apiVersion: "v2.9"});

            $translateProvider.translations('en-US', {
                'Accounts': 'Accounts',
            });

            $translateProvider.translations('ru-RU', {
                'Accounts': 'Аккаунты',
            });

            $translateProvider.preferredLanguage('en-US');

            // Enable escaping of HTML
            $translateProvider.useSanitizeValueStrategy('escape');

            localStorageServiceProvider
                .setPrefix('technical-task')
                .setStorageType('sessionStorage');


            $urlRouterProvider.otherwise('/login');

            $ocLazyLoadProvider.config({
                // Set to true if you want to see what and when is dynamically loaded
                debug: false
            });

            $stateProvider
                .state('login', {
                    url: '/login',
                    templateUrl: 'views/login/index.html',
                    controller: 'LoginCtrl',
                    data: {pageTitle: 'Login', bodyClass: 'gray-bg'},
                    resolve: {
                        loadPlugin: function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: 'technical-task', // app name for the controller proper assignment
                                    files: ['js/controllers/login.js']
                                }
                            ]);
                        }
                    }
                })
                .state('dashboard', {
                    abstract: true,
                    templateUrl: 'views/layout/content.html',
                    controller: 'GlobalCtrl'
                })
                .state('dashboard.index', {
                    url: '/dashboard',
                    templateUrl: 'views/dashboard/index.html',
                    controller: 'DashboardCtrl',
                    data: {pageTitle: 'Dashboard'},
                    resolve: {
                        loadPlugin: function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: 'technical-task',
                                    files: [
                                        'js/controllers/dashboard.js'
                                    ]
                                }
                            ]);
                        }
                    }
                });

        }])
    .run(function ($rootScope, $state, $templateCache, AuthService, SocialUserService) {

        $rootScope.$on('event:social-sign-in-success', function (event, socialUser) {
            SocialUserService.find(socialUser.uid).then(function (user) {
                SocialUserService.update(user._id, socialUser).then(function (user) {
                    AuthService.setUser(user);
                    $state.go('dashboard.index');
                }, function (error) {
                    console.error(error);
                });
            }, function (error) {
                SocialUserService.create(socialUser).then(function (user) {
                    AuthService.setUser(user);
                    $state.go('dashboard.index');
                }, function (error) {
                    console.error(error);
                });
            });
        });

        $rootScope.$state = $state;

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            var user = AuthService.getUser();

            if (toState.name != 'login') {
                if (!user) {
                    event.preventDefault();
                    $state.go('login');
                }
            } else {
                if (user) {
                    event.preventDefault();
                    $state.go('dashboard.index');
                }
            }

            if (user) {
                $rootScope.user = user;
            }

            $templateCache.remove(toState.templateUrl);
        });

    });
