angular.module('technical-task')
    .directive('chosenSelect', function ($rootScope, $timeout) {
        return {
            link: function (scope, element, attributes) {
                $timeout(function () {
                    element.chosen();
                });
            }
        }
    });