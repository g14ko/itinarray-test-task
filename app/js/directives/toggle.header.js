angular.module('technical-task')
    .directive('toggleHeader', function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                title: '@',
                targetSelector: '@',
                expanded: '=?'
            },
            template: '<h4 class="toggle-h4 no-select" role="button"><i style="width:12px;font-size:13px;" class="fa" ng-class="{\'fa-chevron-down\': expanded, \'fa-chevron-right\': !expanded}" aria-hidden="true"></i> {{title}}</h4>',
            link: function (scope, element, attributes) {
                scope.expanded = angular.isDefined(scope.expanded) && scope.expanded;
                angular.element(element).on('click', function (e) {
                    angular.element(scope.targetSelector).toggleClass('hidden');
                    element.find('i.fa').toggleClass('fa-chevron-down', 'fa-chevron-right');
                });
            }
        };
    });