angular.module('technical-task')
    .directive('fooTable', function () {
        return {
            link: function (scope, element, attributes) {
                var ft = null;
                scope.$watchCollection('rows', function () {
                    if (!ft) {
                        ft = FooTable.init(element, {
                            'columns': scope.headers,
                            'rows': scope.rows,
                            'editing': scope.editing
                        });
                    } else {
                        ft.rows.load(scope.rows);
                    }
                });

            }
        }
    });