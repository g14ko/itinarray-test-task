angular.module('technical-task')
    .directive('hideSidebar', function ($rootScope, $timeout) {
        return {
            link: function (scope, element, attributes) {
                var
                    toggleClass = function () {
                        if (element.width() < 769) {
                            $('body').addClass('body-small')
                        } else {
                            $('body').removeClass('body-small')
                        }
                    };

                $(window).bind("load resize", function () {
                    toggleClass();
                });
                $timeout(function () {
                    toggleClass();
                });
            }
        }
    });