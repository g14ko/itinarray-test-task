angular.module('technical-task')
    .directive('fixHeight', function ($timeout) {
        return {
            link: function (scope, element, attributes) {
                var
                    body = $('body'),
                    setHeight = function () {
                        var navbar = $('nav.navbar-default').height();
                        var wrapper = element.height();

                        if (navbar > wrapper) {
                            element.css("min-height", navbar + "px");
                        }

                        if (navbar < wrapper) {
                            element.css("min-height", $(window).height() + "px");
                        }

                        if (body.hasClass('fixed-nav')) {
                            if (navbar > wrapper) {
                                element.css("min-height", navbar - 60 + "px");
                            } else {
                                element.css("min-height", $(window).height() - 60 + "px");
                            }
                        }
                    };

                $(window).bind("load resize scroll", function () {
                    if (!body.hasClass('body-small')) {
                        setHeight();
                    }
                });

                $timeout(function () {
                    setTimeout(function(){
                        setHeight();
                    });
                });
            }
        }
    });




