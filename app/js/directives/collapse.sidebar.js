angular.module('technical-task')
    .directive('collapseSidebar', function ($timeout) {
        return {
            restrict: 'A',
            template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-default" href="" ng-click="collapse()"><i class="fa fa-bars"></i></a>',
            controller: function ($scope, $element) {
                $scope.collapse = function () {
                    var
                        body = $("body"),
                        menu = $("#side-menu");
                    body.toggleClass("mini-navbar");
                    switch (true) {
                        case !body.hasClass('mini-navbar') || body.hasClass('body-small'):
                            // Hide menu in order to smoothly turn on when maximize menu
                            menu.hide();
                            // For smoothly turn on menu
                            setTimeout(function () {
                                menu.fadeIn(400);
                            }, 200);
                            break;
                        case body.hasClass('fixed-sidebar'):
                            menu.hide();
                            setTimeout(function () {
                                menu.fadeIn(400);
                            }, 100);
                            break;
                        default:
                            // Remove all inline style from jquery fadeIn function to reset menu state
                            menu.removeAttr('style');
                    }
                }
            }
        };
    });