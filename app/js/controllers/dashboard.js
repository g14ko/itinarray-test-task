angular.module('technical-task')
    .controller('DashboardCtrl', [
        '$rootScope',
        '$scope',
        '$state',
        '$http',
        '$timeout',
        'AuthService',
        function ($rootScope, $scope, $state, $http, $timeout, AuthService) {

            $scope.pages = [];

            $scope.issetPages = function () {
                return $scope.pages.length > 0;
            };

            $scope.loadPages = function () {
                var user = AuthService.getUser();
                if (user) {
                    FB.api('/' + user.uid + '/accounts?access_token=' + user.token, function (response) {
                        if (response.data) {
                            $scope.pages = response.data;
                        }
                    });
                }
            };

            $timeout(function () {
                $scope.loadPages();
            }, 500);

        }
    ]);