angular.module('technical-task')
    .controller('GlobalCtrl', [
        '$rootScope',
        '$scope',
        '$http',
        '$state',
        '$filter',
        'AuthService',
        function ($rootScope, $scope, $http, $state, $filter, AuthService) {

            $scope.changeLanguage = function (lang) {
                if (lang != AuthService.getLanguage()) {
                    $rootScope.language = AuthService.setLanguage(lang);
                    // UtilService.setHttpHeaders({language: lang});
                    $state.reload();
                }
            };

            $scope.logout = function () {
                AuthService.logout();
                $state.go('login');
            };
        }
    ]);
