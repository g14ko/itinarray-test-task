angular.module('technical-task')
    .controller('LoginCtrl', [
        '$scope',
        '$state',
        'AuthService',
        function ($scope, $state, AuthService) {
            $scope.user = {};

            $scope.login = function () {
                AuthService.login($scope.user)
                    .then(function (data) {
                        AuthService.setUser(data.token);
                        AuthService.setMenu(data.menu);
                        AuthService.setProfile(data.token.scope);
                        $state.go('dashboard.index');
                    }, function (error) {
                        if (error) {
                            toastr.error(error.message, error.name);
                        }
                    });
            };
        }
    ]);