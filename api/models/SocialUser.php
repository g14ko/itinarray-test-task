<?php

namespace app\models;

use Yii;
use yii\mongodb\ActiveRecord;

/**
 * This is the model class for table "social_user".
 *
 * @property int $_id
 * @property string $token
 * @property string $uid
 * @property string $name
 * @property string $email
 * @property string $imageUrl
 * @property string $provider
 * @property string $modified
 */
class SocialUser extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'social_user';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'token',
            'uid',
            'name',
            'email',
            'imageUrl',
            'provider',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['token', 'uid', 'name', 'email', 'imageUrl', 'provider'], 'required'],
            [['modified'], 'safe'],
            [['token', 'uid', 'imageUrl'], 'string', 'max' => 255],
            [['name', 'email'], 'string', 'max' => 64],
            [['provider'], 'string', 'max' => 32],
            [['uid'], 'unique', 'on' => 'create'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'token' => 'Token',
            'uid' => 'Uid',
            'name' => 'Name',
            'email' => 'Email',
            'imageUrl' => 'Image Url',
            'provider' => 'Provider',
            'modified' => 'Modified',
        ];
    }

}
