<?php

return [

    'GET sites' => 'site/index',
    'OPTIONS sites' => 'site/options',
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'site',
        'pluralize' => false,
        'tokens' => ['{id}' => '<id:\d+>'],
        'extraPatterns' => [
            'OPTIONS' => 'options',
            'OPTIONS {id}' => 'options',
        ],
    ],

    'GET social/users' => 'social/user/index',
    'OPTIONS social/users' => 'social/user/options',
    'GET social/user/find/<uid>' => 'social/user/find',
    'OPTIONS social/user/find/<uid>' => 'social/user/options',
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'social/user',
        'pluralize' => false,
        'tokens' => [
            '{id}' => '<id:\w+>',
            '{uid}' => '<uid:\d+>',
        ],
        'extraPatterns' => [
            'OPTIONS' => 'options',
            'OPTIONS {id}' => 'options',
        ],
    ],

];