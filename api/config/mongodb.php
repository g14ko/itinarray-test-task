<?php

$local = file_exists(__DIR__ . '/local/mongodb.php') ? require(__DIR__ . '/local/mongodb.php') : [];

return array_merge([
    'class' => '\yii\mongodb\Connection',
], $local);
