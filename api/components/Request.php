<?php namespace app\components;

use Yii;
use yii\web\NotFoundHttpException;

class Request extends \yii\web\Request
{
    public $errorMessage;

    /**
     * @inheritdoc
     */
    public function resolve()
    {
        if (!empty($this->errorMessage)) {
            try {
                return parent::resolve();
            } catch (NotFoundHttpException $e) {
                throw new NotFoundHttpException(Yii::t('yii', $this->errorMessage));
            }
        }

        return parent::resolve();
    }

}
