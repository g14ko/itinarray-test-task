Download vendors
-------------------
```
npm install
bower install
```

Config API URL
-------------------
In file app/js/config.js find and set apiUrl
```
envServiceProvider.config({
    domains: {
        development: ['localhost', 'acme.dev.local'],
        production: ['acme.com', '*.acme.com', 'acme.dev.prod'],
        test: ['test.acme.com', 'acme.dev.test', 'acme.*.com'],
        // anotherStage: ['domain1', 'domain2']
    },
    vars: {
        development: {
            apiUrl: '//API_URLDEV/',
        },
        test: {
            apiUrl: '//API_URLTEST/',
        },
        production: {
            apiUrl: '//API_URLPROD/',
        },
        defaults: {
            apiUrl: '//api.default.com/v1',
            staticUrl: '//static.default.com'
        }
    }
});
```

Config FB
-------------------
In file app/js/config.js set appID and apiVersion
```
socialProvider.setFbKey({appId: "xxx", apiVersion: "vx.x"});
```

Clean, build and upload application via gulp
-------------------
```
gulp clean
gulp build
gulp upload
```
or 
```
gulp
```