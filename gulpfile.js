const APP = 'app';
const PUBLIC = 'pub';

var gulp = require('gulp');
var assign = require('object-assign');
var useref = require('gulp-useref');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-csso');
var clean = require('gulp-clean');

// clean

gulp.task('clean', function () {
    return gulp.src(PUBLIC + '/*', {read: false})
        .pipe(clean());
});

// build

gulp.task('compile', function () {
    return gulp.src(APP + '/*.html')
        .pipe(useref())
        .pipe(gulpif('*.js', uglify({mangle: false})))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(gulp.dest(PUBLIC));
});

gulp.task('copy-controllers', function () {
    return gulp.src(APP + '/js/controllers/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest(PUBLIC + '/js/controllers/'));
});

gulp.task('copy-views', function () {
    return gulp.src(APP + '/views/**/*.html')
        .pipe(gulp.dest(PUBLIC + '/views'));
});

gulp.task('copy-images', function () {
    return gulp.src(APP + '/img/**/*.{png,jpg}')
        .pipe(gulp.dest(PUBLIC + '/img'));
});

gulp.task('copy-fonts', function () {
    return gulp.src(APP + '/libs/bootstrap/fonts/*.{ttf,woff,woff2,eof,svg}')
        .pipe(gulp.dest(PUBLIC + '/fonts'));
});

gulp.task('copy-font-awesome', function () {
    return gulp.src(APP + '/libs/font-awesome/fonts/*.{ttf,woff,woff2,eof,svg}')
        .pipe(gulp.dest(PUBLIC + '/fonts'));
});

gulp.task('build', [
    'compile',
    'copy-controllers',
    'copy-views',
    'copy-images',
    'copy-fonts',
    'copy-font-awesome'
]);

// upload

var gutil = require('gulp-util');
var ftp = require('gulp-ftp');
var ftpCreds = {
    host: 'zzz.com.ua',
    user: 'xxx',
    pass: 'zzz'
};

gulp.task('upload', function () {
    return gulp.src(PUBLIC + '/**/*')
        .pipe(ftp(assign(ftpCreds, {remotePath: '/admin'})))
        .pipe(gutil.noop());
});

// default

var series = require('@whitneyit/series')(gulp);

gulp.task('default', series(['clean', 'build', 'upload']));